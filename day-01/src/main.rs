use std::io;

fn main() {
    let mut sum_riddle1: u32 = 0;
    let mut sum_riddle2: u32 = 0;

    for line in io::stdin().lines() {
        let ornament: String = line.unwrap();

        sum_riddle1 += calibration_1(&ornament);
        sum_riddle2 += calibration_2(&ornament);
    }

    println!("Riddle 1: sum = {sum_riddle1}");
    println!("Riddle 2: sum = {sum_riddle2}");
}

fn calibration_1(ornament: &String) -> u32 {
    let mut first_digit: char = '_';
    let mut last_digit:  char = '_';

    for c in ornament.chars() {
        if (0x30..0x40).contains(&(c as u32)) {
            if first_digit == '_' {
                first_digit = c;
            }
            last_digit = c;
        }
    }
    return   10 * ((first_digit as u32) - 0x30)
           +      (( last_digit as u32) - 0x30);
}

fn calibration_2(ornament: &String) -> u32 {
    let mut first_digit: i32 = -1;
    let mut last_digit:  i32 = -1;

    for i in  0..ornament.len() {
        let mut digit: i32 = -1;

        let c: char = ornament.chars().nth(i).unwrap();
        if (0x30..0x40).contains(&(c as u32)) {
            digit = ((c as u32) - 0x30) as i32;
        } else if ornament[i..].starts_with("one") {
            digit = 1;
        } else if ornament[i..].starts_with("two") {
            digit = 2;
        } else if ornament[i..].starts_with("three") {
            digit = 3;
        } else if ornament[i..].starts_with("four") {
            digit = 4;
        } else if ornament[i..].starts_with("five") {
            digit = 5;
        } else if ornament[i..].starts_with("six") {
            digit = 6;
        } else if ornament[i..].starts_with("seven") {
            digit = 7;
        } else if ornament[i..].starts_with("eight") {
            digit = 8;
        } else if ornament[i..].starts_with("nine") {
            digit = 9;
        } else if ornament[i..].starts_with("zero") {
            digit = 0;
        }

        if digit != -1 {
            if first_digit == -1 {
                first_digit = digit;
            }
            last_digit = digit;
        }
    }

    return (10 * first_digit + last_digit) as u32;
}
